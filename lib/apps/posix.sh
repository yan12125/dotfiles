alias ll='ls -alh'

alias rm='rm -iv'
alias mv='mv -iv'
alias cp='cp -iv'

alias du='du -h'

alias df='df -h'
