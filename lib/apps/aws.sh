AWS_ZSH_COMPLETER="/usr/bin/aws_zsh_completer.sh"
[[ -n "$AWS_ZSH_COMPLETER" && -f "$AWS_ZSH_COMPLETER" ]] && source "$AWS_ZSH_COMPLETER"

export AWS_CONFIG_FILE="$HOME/.config/aws/config-posix"
