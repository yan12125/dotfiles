$python_executable = (Get-Command python.exe).Source
$python_prefix = (Get-Item $python_executable).DirectoryName
$python_path_segment = (Get-Item $python_prefix).BaseName
$env:Path += ";$env:APPDATA\Python\$python_path_segment\Scripts"

# Use a environment variable as pip on Windows and UNIX-like systems use different config filenames
# https://pip.pypa.io/en/stable/topics/configuration/
# https://github.com/pypa/pip/blob/24.1.1/src/pip/_internal/configuration.py#L32
$env:PIP_CONFIG_FILE = "$DOTFILES\toplevel\config\pip\pip.conf"

$env:PYTHONSTARTUP = "$DOTFILES\lib\pythonrc.py"
