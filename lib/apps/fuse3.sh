# Inspired by https://unix.stackexchange.com/a/723468
# Submit to https://github.com/zsh-users/zsh/blob/master/Completion/Linux/Command/_fusermount ?
compdef _fusermount fusermount3
