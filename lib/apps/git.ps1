# ssh.exe bundled in Git For Windows seems not working with a SSH agent
# Seems msys2 git needs escaping backslashes
$ssh_command = Get-Command ssh
if ($ssh_command) {
    $env:GIT_SSH_COMMAND = $ssh_command.Source.Replace('\', '\\')
}

if (Test-Path Alias:\diff) {
    Remove-Alias diff -Force
}

# Get some useful stuff in bundled MinGW like diff.exe
$git_mingw_bin_path = Join-Path -Resolve (Get-Command git).Source ..\..\usr\bin
$env:Path = "$env:Path;$git_mingw_bin_path"

$env:TIGRC_USER = "$env:USERPROFILE\.osdep\tig"
