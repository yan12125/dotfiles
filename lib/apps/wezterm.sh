# Disable WezTerm shell integrations. It injects several WEZTERM_* env vars,
# and seems not quite useful in my case: https://wezfurlong.org/wezterm/shell-integration.html
export WEZTERM_SHELL_SKIP_ALL=1
