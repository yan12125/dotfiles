export PYTHONSTARTUP=$DOTFILES/lib/pythonrc.py
export MYPY_CACHE_DIR="$HOME/.cache/mypy"

local s virtualenvwrapper_candidates
virtualenvwrapper_candidates=(
    /usr/bin/virtualenvwrapper.sh
    /usr/share/virtualenvwrapper/virtualenvwrapper.sh
)

for s in "${virtualenvwrapper_candidates[@]}"; do
    if [[ -f "$s" ]]; then
        source "$s"
        break
    fi
done
