export NODE_REPL_HISTORY="$DOTFILES_VAR/node_history"

# Avoid ~/.config/configstore/update-notifier-* files created by npm, web-ext, etc.
# https://github.com/yeoman/update-notifier/blob/main/readme.md
export NO_UPDATE_NOTIFIER=1
