# No using --verbose as OpenSSL 3 is now too verbose https://github.com/curl/curl/issues/7843
# Synced with curl.sh
function wget {
    # Using a function as New-Alias does not support arguments (?)
    # https://stackoverflow.com/questions/4166370/how-can-i-write-a-powershell-alias-with-arguments-in-the-middle
    curl --location --remote-name --remote-header-name $args
}
