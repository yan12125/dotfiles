export PYTHON_KEYRING_BACKEND=keyring.backends.SecretService.Keyring
# KEYRING_PROPERTY_SCHEME defines attribute mappings like username -> UserName,
# so that python-keyring can use KeePassXC entries out-of-box
# See: https://github.com/jaraco/keyring/issues/448#issuecomment-1203272383
export KEYRING_PROPERTY_SCHEME=KeePassXC
