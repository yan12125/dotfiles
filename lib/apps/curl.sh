# No using --verbose as OpenSSL 3 is now too verbose https://github.com/curl/curl/issues/7843
# Synced with curl.ps1
alias wget="curl --location --remote-name --remote-header-name"
