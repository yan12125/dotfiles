local android_home_candidate="$HOME/.cache/android-sdk"
if [ -e "$android_home_candidate" ]; then
    export ANDROID_HOME="$android_home_candidate"
    local build_tools_paths=("$ANDROID_HOME/build-tools"/*)
    # Use a special syntax for consistent results in bash and zsh
    # https://stackoverflow.com/questions/50427449/behavior-of-arrays-in-bash-scripting-and-zsh-shell-start-index-0-or-1
    export PATH="$PATH:$ANDROID_HOME/cmdline-tools/latest/bin:$ANDROID_HOME/platform-tools:${build_tools_paths[@]:0:1}"
fi
