alias vim='nvim'
alias nvim='nvim -p'
alias vimdiff='\nvim -d'
export EDITOR=nvim
