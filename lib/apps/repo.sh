[[ -e /usr/share/bash-completion/completions/repo ]] && source /usr/share/bash-completion/completions/repo

# Avoid large TRACE_FILE files
export REPO_TRACE=0
