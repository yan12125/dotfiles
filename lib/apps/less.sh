# Affects man pages, git and maybe more
export LESS_TERMCAP_mb=$'\E[1;31m'      # begin blinking
export LESS_TERMCAP_md=$'\E[4;32m'      # begin bold
export LESS_TERMCAP_me=$'\E[0m'         # end mode
export LESS_TERMCAP_so=$'\E[0;31m'      # begin standout-mode - info box
export LESS_TERMCAP_se=$'\E[0m'         # end standout-mode
export LESS_TERMCAP_us=$'\E[0;33m'      # begin underline
export LESS_TERMCAP_ue=$'\E[0m'         # end underline
export LESSHISTFILE=$DOTFILES_VAR/lesshst
export LESS="--RAW-CONTROL-CHARS --quit-if-one-screen --tabs=4 --mouse --wheel-lines=3"

# Affects systemd stuffs
export SYSTEMD_LESS="$LESS --chop-long-lines"
