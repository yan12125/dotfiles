# zsh accepts bash formats, too
eval $(dircolors -b)
alias ls='ls --color=auto --time-style="+%Y/%m/%d %H:%M:%S"'

# Take advantage of $LS_COLORS for completion as well.
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
