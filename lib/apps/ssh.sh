# $SSH_CONNECTION is passed by default in tmux
# https://github.com/tmux/tmux/blob/3.2a/options-table.c#L713
if [[ -n "$SSH_CONNECTION" && -n "$DISPLAY" ]]; then
    export XAUTHORITY="$XDG_RUNTIME_DIR/xauth-remote"
fi
