# vim: set filetype=gdb:

# add-auto-load-safe-path /home/yen/.gdbinit

set history save
# demangle even in disassemble outputs
set print asm-demangle on

set follow-fork-mode parent
set detach-on-fork on
