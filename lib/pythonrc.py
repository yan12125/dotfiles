# inspired by http://unix.stackexchange.com/questions/121377
# Override things done in Lib/site.py, line 417~429 (version 3.4.1)
import atexit
import os
import os.path
import sys

# Not using $PYTHON_HISTORY introduced in Python 3.13 [1], as I want per-version history files to avoid conflicts between different readline versions
# [1] https://github.com/python/cpython/pull/13208
def move_history_file():
    pyver = '.'.join(map(str, sys.version_info[:2]))
    if 'DOTFILES_VAR' not in os.environ:
        histfile = os.path.join(os.path.expanduser('~'), '.python_history-' + pyver)
    else:
        histfile = os.path.join(os.getenv('DOTFILES_VAR'), './python_history-' + pyver)

    try:
        # Unfortunately, Python 3.13 handles history in a private _pyrepl module
        from _pyrepl import readline

        # Also, CPython is inconsistent: when the original readline module doesn't have history,
        # history is loaded to the new _pyrepl.readline module
        # https://github.com/python/cpython/blob/v3.13.1/Lib/site.py#L549
        # Here I also load history the original readline module to avoid creation of ~/.python_history
        # Note that this should be done only for new Python versions. Otherwise the history is loaded twice.
        import readline as readline_orig
        readline_orig.read_history_file(histfile)
    except ImportError:
        import readline

    try:
        readline.read_history_file(histfile)
    except IOError as e:
        print("Warning: failed to read python history: " + str(e))

    atexit.register(readline.write_history_file, histfile)


move_history_file()
