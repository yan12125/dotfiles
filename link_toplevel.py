#!/usr/bin/env python
import argparse
import errno
import json
import pathlib
import platform
import os
import re
import subprocess


def dirname():
    pwd = os.getenv('PWD')
    if pwd:
        # Prefer $PWD to get shorter link targets in case of complex filesystem layouts
        return pathlib.Path(pwd)
    else:
        return pathlib.Path(__file__).resolve().parent

def toplevel_dir():
    return dirname() / 'toplevel'

CONDITIONS = {
    'nt': os.name == 'nt',
    'posix': os.name == 'posix',
}

def init_conditions():
    # Set conditions based on the SSH version
    ssh_version_output = subprocess.check_output(['ssh', '-V'], stderr=subprocess.STDOUT).decode('utf-8')
    mobj = re.match(r'^OpenSSH_(?:for_Windows_)?([0-9])\.([0-9])p([0-9])', ssh_version_output)
    assert mobj
    ssh_version = tuple(map(int, mobj.groups()))
    CONDITIONS['ssh>=9.2'] = ssh_version >= (9, 2)
    CONDITIONS['ssh<9.2'] = ssh_version < (9, 2)

    # Set conditions based on Linux distributions
    try:
        # https://docs.python.org/3.10/library/platform.html#platform.freedesktop_os_release
        os_release_info = platform.freedesktop_os_release()
    except (OSError, AttributeError):
        # OSError: Cannot read os-release file, such as non-Linux
        # AttributeError: platform.freedesktop_os_release is missing, such as Python < 3.10
        CONDITIONS['debian_like'] = CONDITIONS['non_debian_like'] = False
    else:
        # ID_LIKE is an optional, space-separated list
        # https://www.freedesktop.org/software/systemd/man/latest/os-release.html
        os_ids = [os_release_info['ID']] + os_release_info.get('ID_LIKE', '').split(' ')
        CONDITIONS['debian_like'] = 'debian' in os_ids
        CONDITIONS['non_debian_like'] = 'debian' not in os_ids

    # Set conditions based on tmux version
    try:
        tmux_version_output = subprocess.check_output(['tmux', '-V']).decode('utf-8')
    except FileNotFoundError:
        CONDITIONS['tmux>=3.3'] = CONDITIONS['tmux<3.3'] = False
    else:
        mobj = re.match(r'^tmux ([0-9])\.([0-9])[a-z]?', tmux_version_output)
        assert mobj
        tmux_version_string_matches = mobj.groups()
        tmux_version = tuple(map(int, tmux_version_string_matches))
        CONDITIONS['tmux>=3.3'] = tmux_version >= (3, 3)
        CONDITIONS['tmux<3.3'] = ssh_version < (3, 3)

def link_file(target, link_name):
    home = pathlib.Path.home()

    user = os.getlogin()
    host = re.sub(r'\..*', '', platform.node())
    target = target.replace('%V', str(dirname() / 'var' / f'{user}@{host}'))

    target_path = pathlib.Path(target).expanduser()
    abs_target = (toplevel_dir() / target
                  if not target_path.is_absolute() else target_path)
    abs_link_name = home / link_name

    link_dir = abs_link_name.parent.resolve()
    os.makedirs(link_dir, exist_ok=True)
    if abs_link_name.is_symlink():
        abs_link_name.unlink()
    if not abs_target.exists():
        print(f"Creating folder {abs_target}")
        abs_target.mkdir(parents=True, exist_ok=True)
    try:
        print("Linking %s => %s" % (abs_link_name, abs_target))
        os.symlink(abs_target, abs_link_name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
        print('Warning: File %s exists' % abs_link_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pwd', default='')
    args = parser.parse_args()

    init_conditions()

    if args.pwd:
        os.environ['PWD'] = args.pwd

    cur_dir = dirname()

    cond_dir = cur_dir / 'toplevel_cond'

    linked_targets = set()

    for item in cond_dir.iterdir():
        if item.suffix != '.json':
            continue

        with open(item) as f:
            data = json.load(f)

        for condition, files_to_link in data.items():
            for item in files_to_link:
                target, link_name = item
                if CONDITIONS[condition]:
                    link_file(target, link_name)
                linked_targets.add(target)

    if os.name == 'posix':
        for target in toplevel_dir().iterdir():
            if target.name in linked_targets:
                continue
            link_file(target.name, f'.{target.name}')

if __name__ == '__main__':
    main()
