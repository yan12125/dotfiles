$DOTFILES = (Get-Item (Get-Item $PROFILE).Directory.Target).Parent.Parent.FullName

$env:DOTFILES = $DOTFILES
# Seems the fast one among methods that give case-sensitive results
# https://linuxhint.com/use-powershell-to-get-computer-name/
$env:DOTFILES_VAR = "$DOTFILES\var\$env:USERNAME@" + [System.Net.Dns]::GetHostName()

# https://stackoverflow.com/a/53577474
Set-PSReadlineKeyHandler -Key ctrl+d -Function ViExit

# https://stackoverflow.com/a/43295820
Set-PSReadLineOption -EditMode Emacs

Set-PSReadlineOption -BellStyle None

Set-PSReadLineOption -HistorySearchCursorMovesToEnd

Set-PSReadLineKeyHandler -Chord Ctrl+LeftArrow ShellBackwardWord
Set-PSReadLineKeyHandler -Chord Ctrl+RightArrow ShellForwardWord
# Alt+arrow not working yet https://github.com/PowerShell/PSReadLine/issues/105
# Set-PSReadLineKeyHandler -Chord Alt+LeftArrow BackwardWord
# Set-PSReadLineKeyHandler -Chord Alt+RightArrow ForwardWord
Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward
Set-PSReadLineKeyHandler -Chord Ctrl+w -Function BackwardKillWord
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Based on https://learn.microsoft.com/en-us/windows/terminal/tutorials/new-tab-same-directory
function prompt {
  $loc = $executionContext.SessionState.Path.CurrentLocation;

  $out = ""
  # Print OSC 9;9 command first to work-around a PSReadLine issue
  # https://github.com/PowerShell/PSReadLine/issues/3719
  if ($loc.Provider.Name -eq "FileSystem") {
    $out += "`e]9;9;`"$($loc.ProviderPath)`"`e\"
  }
  $out += "PS $loc$('>' * ($nestedPromptLevel + 1)) ";
  return $out
}

foreach ($script in (Get-ChildItem -Path $DOTFILES\lib\apps -Filter *.ps1)) {
    . $script
}
