import os.path
import subprocess
import sys

# Use a Python wrapper to avoid duplication. I cannot find a portable command for ProxyCommand,
# as that in Win32-OpenSSH uses `posix_spawnp` and does support tilde expansion:
# https://github.com/PowerShell/openssh-portable/blob/v9.5.0.0/sshconnect.c#L239

def main():
    ret = subprocess.call([
        'openssl', 's_client', '-quiet',
                               '-verify_return_error',
                               # `-verify_quiet` is needed as `-quiet` is not really quiet:
                               # https://github.com/openssl/openssl/issues/10213
                               '-verify_quiet',
                               '-servername', sys.argv[1],
                               '-connect', 'server-external.chyen.cc:8443',
                               '-cert', os.path.expanduser('~/.ssh/yen.pem'),
    ])

    sys.exit(ret)

if __name__ == '__main__':
    main()
