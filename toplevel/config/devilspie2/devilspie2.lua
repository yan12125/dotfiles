-- devilspie2 returns an empty string from get_window_property() if
-- the window is found but the property does not exist.
-- http://git.savannah.gnu.org/cgit/devilspie2.git/tree/src/script_functions.c#n1426
need_maximize = (get_window_type() == "WINDOW_TYPE_NORMAL" and get_window_property("WM_TRANSIENT_FOR") == '' and get_window_class() ~= "scrcpy")

window_class_lower = string.lower(get_window_class())

if (window_class_lower == "remote-viewer" or window_class_lower == "virt-manager") and need_maximize then
    -- set_window_position() does not work with maximized window
    unmaximize()
    set_window_position(1920, 0)
end

if need_maximize then
    maximize()
end
