-- EditorConfig

local nvim_version = vim.version()

if nvim_version.major > 0 or (nvim_version.major == 0 and nvim_version.minor >= 9) then
    -- Custom properties for Neovim's builtin editorconfig. See:
    -- https://github.com/neovim/neovim/blob/v0.9.0/runtime/doc/editorconfig.txt
    require('editorconfig').properties.vim_filetype = function(bufnr, val, opts)
        vim.api.nvim_buf_set_option(bufnr, 'filetype', val)
    end
end

local dotfiles_var = os.getenv('DOTFILES_VAR')
if dotfiles_var then
    -- http://stackoverflow.com/a/16706339/3786245
    -- single quote is necessary
    local shada_path = dotfiles_var .. '/main.shada'
    vim.opt.shada = "'1000,n" .. shada_path
end

vim.g.zip_unzipcmd = vim.fn.expand('~/.config/nvim/bsdtar_wrapper.py')

for i = 1, 9 do
    vim.api.nvim_set_keymap('n', '<A-' .. i .. '>', i .. 'gt', { noremap = true, silent = true })
end

if vim.fn.executable('node') == 0 then
    vim.g.coc_start_at_startup = 0
else
    vim.g.coc_start_at_startup = 1
end

if vim.fn.executable('git') == 0 then
    vim.g.gitgutter_enabled = 0
end
