lua require('my_init')

" Force the path to python, so that global pynvim can be used inside virtual environments
if !has('win32')
    let g:python3_host_prog = '/usr/bin/python3'
end

" Basic configurations

syntax on

set number

set mouse=a

set shiftwidth=4

set expandtab
set nowrap
set smartindent

set cursorline " highlight current line
set nostartofline

set wildignorecase

set list
set listchars=tab:>\ ,trail:-,nbsp:+

set title

set nomodeline

set showtabline=2 " show the tab line even if there is only one tab

" Enable preview for :s and similar commands
set inccommand=nosplit

" Restore horizontal wildmenu (default before Neovim 0.4)
set wildoptions-=pum

" Open the new file in right split for vsp and below split for sp
set splitbelow
set splitright

" https://stackoverflow.com/questions/1050640/how-to-stop-vim-from-adding-a-newline-at-end-of-file
set nofixendofline

set fileformats=unix,dos

if !has('win32')
    set directory=$XDG_RUNTIME_DIR/nvim/swap
    set backupdir=$XDG_RUNTIME_DIR/nvim/backup
    " Simplified from https://vim.fandom.com/wiki/Automatically_create_tmp_or_backup_directories
else
    set directory=$TEMP/nvim/swap
    set backupdir=$TEMP/nvim/backup
endif
call mkdir(&backupdir, 'p')

" When editing a file, always jump to the last known cursor position.
" Ref: https://github.com/vim/vim/blob/v8.2.1524/runtime/defaults.vim#L114-L121
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

" clear search register
command CS let @/=""
" Copy the content of search register to clipboard
command CC let @+=printf("'%s'", substitute(@/, '\\[<>]', '\\b', 'g'))

" termguicolors is enabled automatically when supported since Neovim 0.10.0 [1], so guibg is necessary
" https://github.com/neovim/neovim/pull/26407
function SetTransparentBackground()
    highlight Normal ctermbg=None guibg=None
    highlight LineNr ctermbg=None guibg=None
    highlight ColorColumn ctermbg=238

    highlight Underlined ctermfg=81 cterm=underline guifg=#80a0ff gui=underline

    " TabLine changed in Neovim 0.10.0, and it becomes unclear with wombat256mod
    " https://github.com/neovim/neovim/pull/26921
    " Copied from Neovim 0.9.5 settings
    highlight TabLine ctermfg=15 ctermbg=242 cterm=underline guibg=NvimDarkGray4 gui=underline
    highlight TabLineFill cterm=reverse gui=reverse

    highlight StatusLine gui=none
    highlight String     gui=none
    highlight Comment    gui=none
    highlight Todo       gui=none ctermbg=Yellow guibg=Yellow

    highlight! link Operator Statement
    highlight! link NormalFloat Pmenu

    " Customizations
    highlight markdownCode guibg=NvimDarkGray4
endfunction

" http://stackoverflow.com/questions/2440149/override-colorscheme
" http://stackoverflow.com/questions/1413285/multiple-autocommands-in-vim
autocmd ColorScheme * call SetTransparentBackground()

if $TERM == "linux"
    " Highlighted texts are displayed normally in this mode
    colorscheme desert
else
    colorscheme wombat256mod
endif

set fileencoding=utf-8
" sjis: Shift-JIS
" http://stackoverflow.com/questions/4103970/how-to-read-sjis-encoded-file-in-vim
set fileencodings=utf-8,big5,ucs-bom,sjis,gb18030,latin1

" Matching moving shortcuts in .zshrc
nnoremap <C-Left> B
nnoremap <C-Right> W
nnoremap <A-Left> b
nnoremap <A-Right> w
inoremap <C-Left> <C-o>B
inoremap <C-Right> <C-o>W
inoremap <A-Left> <C-o>b
inoremap <A-Right> <C-o>w

" Ctrl+click opens the URL in Konsole - make it dummy in vim
noremap <C-LeftMouse> <LeftMouse>

" tar

let g:tar_cmd = 'bsdtar'

" gitgutter
" https://github.com/airblade/vim-gitgutter#signs-colours-and-symbols

highlight GitGutterAdd    ctermfg=2 ctermbg=none guifg=NvimLightGreen  guibg=none
highlight GitGutterChange ctermfg=3 ctermbg=none guifg=NvimLightYellow guibg=none
highlight GitGutterDelete ctermfg=1 ctermbg=none guifg=NvimLightRed    guibg=none

" https://github.com/airblade/vim-gitgutter#sign-column
highlight! link SignColumn LineNr

" Syntastic

let g:syntastic_python_checkers = [ 'flake8' ]
let g:syntastic_tex_checkers = [ 'chktex' ]
let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "passive_filetypes": ["c", "cpp", "python", "java"] }

" vim-airline

let g:airline#extensions#whitespace#checks = []
" Somehow this extension makes selection in large files (e.g., 10~100MB) slow
let g:airline#extensions#wordcount#enabled = 0
" Somehow searchcount() built-in function, which is used by the airline
" searchcount extension, is very slow for large files
let g:airline#extensions#searchcount#enabled = 0

if g:coc_start_at_startup
    " coc.nvim
    " Many are based on https://github.com/neoclide/coc.nvim#example-vim-configuration
    " GoTo code navigation
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gr <Plug>(coc-references)
    nmap <silent> [g <plug>(coc-diagnostic-next)
    nmap <silent> ]g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]e <Plug>(coc-diagnostic-next-error)
    nmap <silent> [e <Plug>(coc-diagnostic-prev-error)
    " Apply the most preferred quickfix action to fix diagnostic on the current line
    nmap <leader>qf  <Plug>(coc-fix-current)
    " https://github.com/neoclide/coc.nvim/pull/3862
    " Inspired by https://github.com/serena-w/dotfiles/commit/b37fe7d47f52cc7d9cb08d4787f2bc5d5a5263fc
    inoremap <silent><expr> <tab> coc#pum#visible() ? coc#pum#confirm() : "\<tab>"
    " Inspired by https://github.com/neoclide/coc.nvim/discussions/3999#discussioncomment-3725039
    highlight link CocMenuSel PmenuSel
    highlight link CocPumMenu Pmenu
    highlight link CocPumVirtualText Comment
endif

autocmd BufNewFile,BufRead *.cpp,*.c,*.h highlight Identifier ctermfg=none

autocmd BufNewFile,BufRead *.jsm set filetype=javascript
autocmd BufReadCmd *.apk,*.whl,*.aar,*.nupkg call zip#Browse(expand("<amatch>"))
autocmd BufReadCmd *.tbz2,*.7z call tar#Browse(expand("<amatch>"))
autocmd BufNewFile,BufRead PKGBUILD set filetype=sh shiftwidth=2
autocmd BufNewFile,BufRead *.plt set filetype=gnuplot
autocmd BufNewFile,BufRead *.proto3 set filetype=proto
autocmd BufNewFile,BufRead Portfile set filetype=tcl
autocmd BufNewFile,BufRead *.j2 set filetype=jinja
autocmd BufNewFile,BufRead *.tex,*.md set wrap
let g:tex_flavor = 'latex'

autocmd BufNewFile,BufRead * call InitCppIncludeFiles()

function InitCppIncludeFiles()
    if(match(expand("%:p"), '/usr/include/c++/')>=0)
        set tabstop=8
        set filetype=cpp
    endif
endfunction

if $VIM_MINIMAL != 1
    let &packpath .= ',' . expand('~/.config/nvim/optional')

    exec 'py3file ' . expand('<sfile>:p:h') . '/init.py'

    " http://blog.realnitro.be/2010/12/20/format-json-in-vim-using-pythons-jsontool-module/
    command FormatJs call s:transform_buffer('js_beautify')
    command FormatJson call s:transform_buffer('format_json')
    command FormatXml call s:transform_buffer('format_xml')
    command FormatQueryString call s:transform_buffer('format_qs')
    command UnescapeHTML call s:transform_buffer('unescape_html')
    command UnquoteURI call s:transform_buffer('unquote_uri')
    command TabeFilePath call s:tabe_file_path()

    function s:transform_buffer(func_name)
    python3 << endpython
transformer = globals()[vim.eval('a:func_name')]
transform_buffer(transformer)
endpython
    endfunction

    function s:tabe_file_path()
    python3 << endpython
vim.command('tabe ' + vim.call('expand', '%:p:h'))
endpython
    endfunction
endif
