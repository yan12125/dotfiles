import html
import json
import os
import os.path
import shutil
import sys
import urllib.parse
import vim


def transform_buffer(transformer):
    buf = vim.current.buffer
    new_data = transformer('\n'.join(buf[:]))
    if new_data is not None:
        buf[:] = new_data.split('\n')


def set_cursor(line, col):
    vim.call('cursor', line, col)


def print_error(msg):
    # vim.err_write does not flush things, so the message does not appear
    # and vim has redirected stderr to its error output
    print(str(msg), file=sys.stderr)


def _pretty_json(obj):
    # Copied from json/tool.py
    return json.dumps(obj, sort_keys=True, indent=4, separators=(',', ': '),
                      ensure_ascii=False)


def format_json(data):
    try:
        obj = json.loads(data)
    except json.JSONDecodeError as e:
        set_cursor(e.lineno, e.colno)
        print_error(str(e))
        return

    return _pretty_json(obj)


def format_xml(data):
    import lxml.etree

    # http://lxml.de/FAQ.html#why-doesn-t-the-pretty-print-option-reformat-my-xml-output
    parser = lxml.etree.XMLParser(remove_blank_text=True)
    try:
        doc = lxml.etree.fromstring(data.encode('utf-8'), parser=parser)
    except lxml.etree.XMLSyntaxError as e:
        # XXX: e.position is not documented
        x, y = e.position
        set_cursor(x, y)
        print_error(str(e))
        return

    # by default tostring returns bytes
    # http://lxml.de/api/lxml.etree-module.html#tostring
    return lxml.etree.tostring(
        doc, encoding='utf-8', pretty_print=True, xml_declaration=True,
    ).decode('utf-8')


def format_qs(data):
    return _pretty_json(urllib.parse.parse_qs(data))


def unescape_html(data):
    return html.unescape(data)


def unquote_uri(data):
    return urllib.parse.unquote_plus(data)


def js_beautify(data):
    import jsbeautifier

    opts = jsbeautifier.default_options()
    opts.keep_function_indentation = True  # -f
    return jsbeautifier.beautify(data)
