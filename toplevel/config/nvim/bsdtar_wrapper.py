#!/usr/bin/env python3

import os
import sys

print(sys.argv)
if len(sys.argv) == 5 and sys.argv[1:4] == ['-Z', '-1', '--']:
    os.execvp('bsdtar', ['bsdtar', '-tf', sys.argv[4]])
elif len(sys.argv) == 5 and sys.argv[1:3] == ['-p', '--']:
    os.execvp('bsdtar', ['bsdtar', '-xOf', sys.argv[3], sys.argv[4]])
else:
    print('Unknown options', file=sys.stderr)

# If this line is hit, either options are not supported or os.execvp() failed
sys.exit(1)
