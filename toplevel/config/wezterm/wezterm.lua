local wezterm = require 'wezterm'

local config = {}

config.window_decorations = 'NONE'
config.hide_tab_bar_if_only_one_tab = true
config.hide_mouse_cursor_when_typing = false

-- https://wezfurlong.org/wezterm/config/fonts.html
-- wezterm not using ~/.config/fontconfig/fonts.conf? Somehow autohint and the default font are not applied
local default_fonts = { 'DejaVu Sans Mono', 'Noto Sans CJK TC' }
config.font = wezterm.font_with_fallback(default_fonts)
config.font_size = 14
config.freetype_load_flags = 'FORCE_AUTOHINT' 
config.line_height = 1.05

config.warn_about_missing_glyphs = false

-- https://wezfurlong.org/wezterm/config/lua/config/background.html
config.background = {
    {
        source = { Color = 'black' },
        width = '100%',
        height = '100%',
    },
    {
        source = { File = '/home/yen/Pictures/wallpaper/Narzisse_1920_flop_alpha25.png' },
        opacity = 1.0
    },
}

config.colors = {
    foreground = '#b2b2b2',
    background = '#000000',

    ansi = {
        '#000000',
        '#dc322f',
        '#47c047',
        '#b26818',
        '#3465a4',
        '#b218b2',
        '#18b2b2',
        '#b2b2b2'
    },
    brights = {
        '#686868',
        '#cb4b16',
        '#8ae234',
        '#ffff54',
        '#739fcf',
        '#ff54ff',
        '#54ffff',
        '#ffffff'
    },

    -- Reversed colors for selections are not supported yet, so use current
    -- foreground and background colors.
    -- See: https://github.com/wez/wezterm/pull/4093
    selection_bg = '#b2b2b2',
    selection_fg = '#000000',
}

-- wezterm does not support something like foreground_bright yet, thus the workaround
-- https://github.com/wez/wezterm/issues/3415
config.font_rules = {
    {
        intensity = 'Bold',
        font = wezterm.font_with_fallback(
        default_fonts,
        { weight = 'Bold', foreground = '#d3d3d7' }
        )
    },
}

config.keys = {
    -- Disable some default actions used in tmux/vim/...
    -- See https://wezfurlong.org/wezterm/config/default-keys.html
    { key = 'PageUp', mods = 'CTRL', action = wezterm.action.DisableDefaultAssignment },
    { key = 'PageDown', mods = 'CTRL', action = wezterm.action.DisableDefaultAssignment },
    { key = 'PageUp', mods = 'SHIFT|CTRL', action = wezterm.action.DisableDefaultAssignment },
    { key = 'PageDown', mods = 'SHIFT|CTRL', action = wezterm.action.DisableDefaultAssignment },
    { key = 'W', mods = 'SHIFT|CTRL', action = wezterm.action.DisableDefaultAssignment },
    { key = 'w', mods = 'SUPER', action = wezterm.action.DisableDefaultAssignment },
    -- XXX: Disable all default key bindings?

    { key = 'Home', mods = 'SHIFT', action = wezterm.action.ScrollToTop },
    { key = 'End', mods = 'SHIFT', action = wezterm.action.ScrollToBottom },
}

config.mouse_bindings = {
    -- Bind for both mouse_reporting=true and mouse_reporting=false, so that the action
    -- is applied both inside and outside mouse-capturing applications (ex: vim, tmux, ...)
    -- See https://wezfurlong.org/wezterm/config/mouse.html
    {
        event = { Up = { streak = 1, button = 'Left' } },
        mods = 'SHIFT',
        action = wezterm.action.DisableDefaultAssignment,
        mouse_reporting = true,
    },
    {
        event = { Up = { streak = 1, button = 'Left' } },
        mods = 'SHIFT',
        action = wezterm.action.DisableDefaultAssignment,
        mouse_reporting = false,
    },

    {
        event = { Down = { streak = 1, button = 'Left' } },
        mods = 'CTRL',
        action = wezterm.action.OpenLinkAtMouseCursor,
        mouse_reporting = true,
    },
    {
        event = { Down = { streak = 1, button = 'Left' } },
        mods = 'CTRL',
        action = wezterm.action.OpenLinkAtMouseCursor,
        mouse_reporting = false,
    },
}

-- https://wezfurlong.org/wezterm/config/lua/config/hyperlink_rules.html
-- https://www.ietf.org/rfc/rfc3986.txt
-- https://stackoverflow.com/a/45872342
-- https://github.com/wez/wezterm/blob/20230712-072601-f4abf8fd/config/src/config.rs#L1616
-- https://github.com/wez/wezterm/blob/20230712-072601-f4abf8fd/termwiz/src/hyperlink.rs#L300
config.hyperlink_rules = {
    -- Then handle URLs not wrapped in brackets
    {
        -- Excluding ( ) ~ . ' at the end to avoid matching unneeded characters in some common cases:
        -- * Markdown ~~strikethrough texts~~
        -- * [Markdown links](url).
        -- * 'URLs as a string'
        regex = [[\b\w+://[a-zA-Z0-9-._~%:\/?#\[\]@!$&'()*+,;=]+[a-zA-Z0-9-._%:\/?#\[\]@!$&*+,;=]+]],
        format = '$0',
    },
}

config.window_close_confirmation = 'AlwaysPrompt'
-- By default, close confirmation for some processes (ex: zsh, tmux) are skipped,
-- while I want close confirmation for everyone.
config.skip_close_confirmation_for_processes_named = {}

return config
