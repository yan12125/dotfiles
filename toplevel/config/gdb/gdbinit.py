# vim: set filetype=python:

# http://stackoverflow.com/questions/14226563/how-to-read-and-execute-gdb-commands-from-a-file
# http://stackoverflow.com/questions/16553283/python-code-in-gdb-init-file-gdbinit
import os
import os.path
import gdb  # not needed. To make flake8 happy
import glob


def gdb_source(path):
    gdb.execute('source ' + path)

if os.getenv('GDB_ENABLE_DEBUGINFOD') == '1':
    gdb.execute('set debuginfod enabled on')
else:
    gdb.execute('set debuginfod enabled off')

dirname = os.environ["DOTFILES"]
dotfiles_var = os.environ["DOTFILES_VAR"]

gdb_source(os.path.join(dirname, './third_party/gdbinit_stl/.gdbinit'))
for f in glob.glob(os.path.join(dirname, './lib/gdb/*.gdbinit')):
    gdb_source(f)

# http://stackoverflow.com/questions/3176800
gdbhist_path = os.path.join(dotfiles_var, "./gdb_history")
gdb.execute('set history filename ' + gdbhist_path)
gdb.execute('set history size 10000')

arch = gdb.execute('show architecture', to_string=True)

if 'i386' in arch:
    gdb.execute('set disassembly-flavor intel')
