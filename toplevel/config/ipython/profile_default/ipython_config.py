import os.path

c = get_config()
c.TerminalInteractiveShell.debugger_history_file = os.path.expanduser('~/.local/share/pdbhistory')
