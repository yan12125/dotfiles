import os
import subprocess
import sysconfig

def main():
    possible_scripts_paths = [
        sysconfig.get_path(name='scripts', scheme=sysconfig.get_preferred_scheme(scheme))
        for scheme in ('user', 'prefix')
    ]
    for scripts_path in possible_scripts_paths:
        pinentry_kpxc_path = os.path.join(scripts_path, 'pinentry-kpxc.exe')
        if os.path.exists(pinentry_kpxc_path):
            break
    else:
        raise RuntimeError('Cannot find pinentry-kpxc')
    gpg_cmd = [
        'gpg-agent',
        '--daemon',
        '--enable-win32-openssh-support',
        '--pinentry-program', pinentry_kpxc_path,
    ]

    # Hide the console for gpg-agent
    # https://stackoverflow.com/questions/7006238/how-do-i-hide-the-console-when-i-use-os-system-or-subprocess-call
    # https://learn.microsoft.com/en-us/windows/win32/api/processthreadsapi/ns-processthreadsapi-startupinfow
    si = subprocess.STARTUPINFO()
    si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    si.wShowWindow = subprocess.SW_HIDE

    subprocess.Popen(gpg_cmd, startupinfo=si)

if __name__ == '__main__':
    main()
