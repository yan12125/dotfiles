#!/usr/bin/python
import logging
import os
import os.path
import subprocess

import dbus

def click_synergy_menu(dbus_connection_name, shortcut: list[str]):
    # Simulating clicking on the tray menu
    bus = dbus.SessionBus()
    try:
        obj = bus.get_object(dbus_connection_name, '/MenuBar')
        menu_interface = dbus.Interface(obj, 'com.canonical.dbusmenu')

        # https://github.com/unity8-team/libdbusmenu-qt/blob/0.9.3+16.04.20160218-0ubuntu1/src/com.canonical.dbusmenu.xml#L186-L225
        _, layout = menu_interface.GetLayout(
            0,                                  # parentId
            -1,                                 # recursionDepth
            dbus.Array([], signature='s'),      # propertyNames
        )

        _, _, submenu_layout = layout

        for item in submenu_layout:
            idx, properties, _ = item
            shortcuts = properties.get('shortcut', [])
            if shortcut in shortcuts:
                target_item_idx = idx
                break

        # https://github.com/unity8-team/libdbusmenu-qt/blob/0.9.3+16.04.20160218-0ubuntu1/src/com.canonical.dbusmenu.xml#L274-L298
        menu_interface.Event(
            int(target_item_idx),  # id
            'clicked',  # eventId
            '',         # data
            0,          # timestamp
        )

        return True
    except dbus.DBusException:
        # synergy also owns some other connections without the dbusmenu interface
        return False

def find_dbus_connection(binary_name):
    # Find a DBus connection owned by synergy
    bus = dbus.SessionBus()
    obj = bus.get_object('org.freedesktop.DBus', '/org/freedesktop/DBus')
    for name in obj.ListNames():
        connection_name = obj.GetNameOwner(name)
        pid = obj.GetConnectionUnixProcessID(connection_name)
        try:
            exe = os.readlink(f'/proc/{pid}/exe')
        except PermissionError:
            # Protected processes like keepassxc
            continue
        if os.path.basename(exe) == binary_name:
            yield connection_name

def main():
    # Suppress dbus errors when inspecting fails
    dbus_logger = logging.getLogger('dbus')
    dbus_logger.setLevel(logging.CRITICAL)

    try:
        subprocess.check_call(['xsecurelock'])
    except subprocess.CalledProcessError:
        # xsecurelock may fail to grab inputs if synergy already does that - stop synergy and try again
        synergy_connection_name = None
        for connection_name in find_dbus_connection('synergy'):
            # xsecurelock probably fails to grab as synergy already does that
            # Try to stop synergy and lock the screen again
            if click_synergy_menu(connection_name, ['Control', 'T']):
                synergy_connection_name = connection_name
        proc = subprocess.Popen(['xsecurelock'])
        # Restart synergy when xsecurelock is run
        click_synergy_menu(synergy_connection_name, ['Control', 'S'])
        proc.wait()

if __name__ == '__main__':
    main()
