from datetime import datetime, timedelta, timezone
import json
import logging
import subprocess

logger = logging.getLogger('restic_wrapper')

def main():
    logging.basicConfig(level=logging.INFO)

    resticprofile_bin = '/usr/bin/resticprofile'
    snapshots = json.loads(subprocess.check_output([
        resticprofile_bin, '--stderr', 'snapshots', '--json'
    ]).decode('utf-8'))

    now = datetime.now(timezone.utc)
    latest_snapshot_time = datetime.fromisoformat(snapshots[-1]['time'])

    if now - latest_snapshot_time < timedelta(days=1):
        logger.info('Skipping - latest snapshot time %s is not older than 1 day', latest_snapshot_time)
        return

    backup_cmd = [resticprofile_bin, 'backup']
    print('Running %s', backup_cmd)

    subprocess.check_call(backup_cmd)

if __name__ == '__main__':
    main()
