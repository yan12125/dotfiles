#!/bin/sh

SRC_BASE_PATH="$HOME/var/Android/Android/data/jp.naver.line.android"
DST_PATH="$HOME/Pictures/line"

# gravatars
cp -av --no-clobber "$SRC_BASE_PATH/storage/p"/* "$DST_PATH"
# images/files in messages
cp -av --no-clobber "$SRC_BASE_PATH/files/chats"/*/messages/* "$DST_PATH"
