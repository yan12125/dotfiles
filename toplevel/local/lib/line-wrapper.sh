# Hide Noto Color Emoji in LINE to workaround https://bugs.winehq.org/show_bug.cgi?id=53795
exec bwrap --dev-bind / / --tmpfs /usr/share/fonts/noto wine "C:\\\\users\\\\yen\\\\AppData\\\\Local\\\\LINE\\\\bin\\\\current\\\\LINE.exe" run > /dev/null 2>&1
